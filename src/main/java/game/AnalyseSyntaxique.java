package game;

import java.util.ArrayList;
import java.util.List;

public class AnalyseSyntaxique {

	private int pos;
	private List<Token> tokens;
/*

	méthodes utilitaires

	 */

	private boolean isEOF() {
		return pos >= tokens.size();
	}

	/*
	 * Retourne la classe du prochain token à lire
	 * SANS AVANCER au token suivant
	 */
	private TokenClass getTokenClass() {
		if (pos >= tokens.size()) {
			return null;
		} else {
			return tokens.get(pos).getCl();
		}
	}

	/*
	 * Retourne le prochain token à lire
	 * ET AVANCE au token suivant
	 */
	private Token getToken() {
		if (pos >= tokens.size()) {
			return null;
		} else {
			Token current = tokens.get(pos);
			pos++;
			return current;
		}
	}
	/*
	 * Retourne le prochain token à lire
	 * ET AVANCE au token suivant
	 */
	private Token getBackToken() {
		if (pos <= 0 ) {
			return null;
		}
		if(pos >= tokens.size()){
			pos--;
			Token current = tokens.get(pos);
			return current;
		}
		else {
			Token current = tokens.get(pos);
			pos--;
			return current;
		}
	}
	/*
	effectue l'analyse syntaxique à partir de la liste de tokens
	et retourne le noeud racine (type Node) de l'arbre syntaxique abstrait
	 */
	public Node analyse(List<Token> tokens) throws Exception {
		pos = 0;
		this.tokens = tokens;
		Node n = new Node(NodeClass.statement);
		Node res = S(n);
		if (pos != tokens.size()) {
			throw new IncompleteParsingException();
		}
		return res;
	}

	/*

	Traite la dérivation du symbole non-terminal S

	S ->  A S'

	 */

	private Node S(Node i) throws UnexpectedTokenException {
		if (	getTokenClass() == TokenClass.kFantome ||
				getTokenClass() == TokenClass.kTuile ||
				getTokenClass() == TokenClass.kVisite){
			Node a = new Node(NodeClass.A);
			a.append(A());
			i.append(a);
		}
		else {
			throw new UnexpectedTokenException("'fantome' ou 'tuile' ou 'visite' attendu");
		}
//		System.out.println("2 eme fonction: "+ getTokenClass());
		if(getTokenClass() == TokenClass.semicolon ||
					getTokenClass() == TokenClass.kFantome ||
					getTokenClass() == TokenClass.kTuile ||
					getTokenClass() == TokenClass.kVisite){
			S_prime(i);
		}
		return i;
	}

	/*

	Traite la dérivation du symbole non-terminal S'

	S' -> S | epsilon | ;

	 */

	private Node S_prime(Node i) throws UnexpectedTokenException {
		if (isEOF()) return null;
		if(getTokenClass() == TokenClass.kFantome || getTokenClass() == TokenClass.kTuile ||
				getTokenClass() == TokenClass.kVisite || getTokenClass() == TokenClass.semicolon){
			return S(i);
		}
	throw new UnexpectedTokenException("SPrime");
	}
	/*

	Traite la dérivation du symbole non-terminal A

	A -> Lettre

	 */

	private Node A() throws UnexpectedTokenException {
		return Lettre();
	}
	/*

	Traite la dérivation du symbole non-terminal Lettre

	Lettre -> kFantome( intVal, intVal, IntVal) | kFantome( intVal, intVal, IntVal, ( Mouvement )) | kTuile(Position ; Mur) | kVisite(Position) to Etat

	 */

	private Node Lettre() throws UnexpectedTokenException {
		Token t = getToken();
//		System.out.println(t.getCl());
		// Lettre -> kFantome( intVal, intVal, IntVal) | kFantome( intVal, intVal, IntVal, ( Mouvement ))

		if (t.getCl() == TokenClass.kFantome) {
			/* récupère le token suivant */
			Token t1 = getToken();
			if (t1.getCl() == TokenClass.leftPar) {
				/* récupère le token suivant */
				Token t2 = getToken();
				if (t2.getCl() == TokenClass.intVal) {
					/* récupère le token suivant */
					Token t3 = getToken();
					if (t3.getCl() == TokenClass.comma) {
						/* récupère le token suivant */
						Token t4 = getToken();
						if (t4.getCl() == TokenClass.intVal) {
							/* récupère le token suivant */
							Token t5 = getToken();
							if (t5.getCl() == TokenClass.comma) {
								/* récupère le token suivant */
								Token t6 = getToken();
								if (t6.getCl() == TokenClass.intVal) {
									/* récupère le token suivant */
									Token t7 = getToken();
									if (t7.getCl() == TokenClass.comma) {
										/* récupère le token suivant */
										Token t8 = getToken();
										if (t8.getCl() == TokenClass.leftPar) {
											/* récupère le token suivant */
											Token t9 = getToken();
											if (t9.getCl() == TokenClass.dDevant ||
													t9.getCl() == TokenClass.dGauche ||
													t9.getCl() == TokenClass.dDroite ||
													t9.getCl() == TokenClass.dZero ||
													t9.getCl() == TokenClass.dArriere || isEOF()) {
												Node s = new Node(NodeClass.Mouvement);
												Node m = Mouvement(s);
												/* récupère le token suivant */
												Token t10 = getToken();
												if (t10.getCl() == TokenClass.rightPar) {
													/* récupère le token suivant */
													Token t11 = getToken();
													if (t11.getCl() == TokenClass.rightPar) {
														Node n = new Node(NodeClass.kFantome);
														n.append(new Node(NodeClass.intVal, t2.getValue()));
														n.append(new Node(NodeClass.intVal, t4.getValue()));
														n.append(new Node(NodeClass.intVal, t6.getValue()));
														n.append(m);
														return n;
													}
												}
											}
										}
									}
									else{
									Node n = new Node(NodeClass.kFantome);
									n.append(new Node(NodeClass.intVal, t2.getValue()));
									n.append(new Node(NodeClass.intVal, t4.getValue()));
									n.append(new Node(NodeClass.intVal, t6.getValue()));
									return n;
								}
								}
							}
						}
					}
				}
			}
		}
		// Lettre -> kTuile(Position ; Mur)
		if (t.getCl() == TokenClass.kTuile) {
			/* récupère le token suivant */
			Node k = new Node(NodeClass.kTuile);
			Token t1 = getToken();
			if (t1.getCl() == TokenClass.leftPar) {
				/* récupère le token suivant */
				Token t2 = getToken();
				if (t2.getCl() == TokenClass.intVal) {
					/* récupère le token suivant */
					Node p = Position(k); //Position de la sortis
					Token t3 = getToken();
					if (t3.getCl() == TokenClass.semicolon) {
						/* récupère le token suivant */
						Token t4 = getToken();
						if (t4.getCl() == TokenClass.intVal || t4.getCl() == TokenClass.semicolon || isEOF()) {
							/* récupère le token suivant */
							Node m = Mur(new Node(NodeClass.Mur));
							k.append(m); //Position de la sortis des murs
							return k;
						}
					}
				}
			}
		}
		// Lettre -> kVisite(Position) to Etat
		if (t.getCl() == TokenClass.kVisite) {
			Node n = new Node(NodeClass.kVisite);
			/* récupère le token suivant */
			Token t1 = getToken();
			if (t1.getCl() == TokenClass.leftPar) {
				/* récupère le token suivant */
				Token t2 = getToken();
				if (t2.getCl() == TokenClass.intVal) {
					Position(n);
					/* récupère le token suivant */
					Token t3 = getToken();
					if (t3.getCl() == TokenClass.rightPar) {
						/* récupère le token suivant */
						Token t4 = getToken();
						if (t4.getCl() == TokenClass.to) {
							/* récupère le token suivant */
							Token t5 = getToken();
							if (t5.getCl() == TokenClass.intVal || isEOF()) {
								getBackToken();
								Node e = Etat(n);
								return e;
							}
						}
					}
				}
			}
		}
		throw new UnexpectedTokenException("'fantome' ou 'tuile' ou 'visite' attendu");
	}
	/*

	Traite la dérivation du symbole non-terminal Etat

	Etat -> Position Etat’ ; Etat | epsilone

	 */

	private Node Etat(Node i) throws UnexpectedTokenException {
		if (getTokenClass() == TokenClass.intVal) {
			getToken();
			Node p = Position(i);
			p.append(EtatPrime());
			getToken();
			Etat(p);
			return p;
		}
		if(isEOF()){ // Ne passe jamais par ici ?
			Node n = new Node(NodeClass.Mur);
			return n;
		}
		return i;
	}

	/*

	Traite la dérivation du symbole non-terminal EtatPrime

	EtatPrime -> tMur | tVide

	 */
	private Node EtatPrime() throws UnexpectedTokenException {
		Token t = getToken();
		if(t.getCl() == TokenClass.tMur) return new Node(NodeClass.tMur);
		if(t.getCl() == TokenClass.tVide) return new Node(NodeClass.tVide);
		throw new UnexpectedTokenException("EtatPrime");
	}

	/*

	Traite la dérivation du symbole non-terminal Position

	Position -> intVal, intVal

	 */
	private Node Position(Node i) throws UnexpectedTokenException {
		getBackToken();
		Token t = getToken();
		getToken();
		Token t1 = getToken();
		Node p = new Node(NodeClass.Position);
		p.append(new Node(NodeClass.intVal, t.getValue()));
		p.append(new Node(NodeClass.intVal, t1.getValue()));
		i.append(p);
		return i;
	}

	/*
	Traite la dérivation du symbole non-terminal Mur

	Mur -> intVal Mur’ Symbole Mur | ε

	 */
	private Node Mur(Node i) throws UnexpectedTokenException {
		getBackToken();
		Token t = getToken();
		if (t.getCl() == TokenClass.intVal) {
			Node n = new Node(NodeClass.intVal, t.getValue());
			Node q = new Node(NodeClass.bloc);
			q.append(n);
			Token t1 = getToken();
			if (t1.getCl() == TokenClass.to || t1.getCl() == TokenClass.comma || t1.getCl() == TokenClass.semicolon || t1.getCl() == TokenClass.rightPar) { // ?
				Node m = MurPrime(q);
				Token t2 = getToken();
				if (t2.getCl() == TokenClass.semicolon ||
						t2.getCl() == TokenClass.comma ||
						t2.getCl() == TokenClass.intVal) {
					Node s = Symbole(m);
					Token t3 = getToken();
					if (t3.getCl() == TokenClass.intVal || t3.getCl() == TokenClass.rightPar || t3.getCl() == TokenClass.semicolon) { // Follow Mur ?
						i.append(s);
						Node x = Mur(i);
						return x;
					}
				}
			}
		}
		if(t.getCl() == TokenClass.semicolon){
			Node s = new Node(NodeClass.bloc);
			s.append(new Node(NodeClass.semicolon));
			getBackToken();
			Token t3 = getToken();
			getToken();
			if (t3.getCl() == TokenClass.intVal || t3.getCl() == TokenClass.rightPar || t3.getCl() == TokenClass.semicolon) { // Follow Mur ?
						i.append(s);
						Node x = Mur(i);
						return x;
					}
			i.append(s);
			return i;
		}
		if(t.getCl() == TokenClass.rightPar) {
			i.append(new Node(NodeClass.semicolon));
			return i;
		}
		throw new UnexpectedTokenException("Mur");
	}

	/*
	Traite la dérivation du symbole non-terminal MurPrime

	MurPrime -> to intVal |  ε

	 */
	private Node MurPrime(Node p) throws UnexpectedTokenException {
		getBackToken();
		Token t = getToken();
		if (t.getCl() == TokenClass.to) {
			Token t1 = getToken();
			if (t1.getCl() == TokenClass.intVal) {
				p.append(new Node(NodeClass.intVal, t1.getValue()));
				return p;
			}
		}
		if(t.getCl() == TokenClass.comma || t.getCl() == TokenClass.semicolon || t.getCl() == TokenClass.rightPar || t.getCl() == TokenClass.tMur || t.getCl() == TokenClass.tVide){
			getBackToken();
			return p;
		}
		throw new UnexpectedTokenException("MurPrime");
	}

	/*
	Traite la dérivation du symbole non-terminal Symbole

	Symbole -> ; | , | ε

	 */
	private Node Symbole(Node i) throws UnexpectedTokenException {
		getBackToken();
		Token t = getToken();
		if(t.getCl() == TokenClass.semicolon) {
			Node n = new Node(NodeClass.semicolon);
			i.append(n);
			return i;
		}
		if(t.getCl() == TokenClass.comma) return i;
		if(isEOF()) return i;

		throw new UnexpectedTokenException("Symbole");
	}

	/*
	Traite la dérivation du symbole non-terminal Mouvement

	Mouvement ->  dDevant,intVal  ; Mouvement |
	 dGauche,intVal  ; Mouvement |
	  dDroite,intVal ; Mouvement |
	   dZero,intVal ; Mouvement|
	    ε
	 */
	private Node MouvementF(Node i, NodeClass node) throws UnexpectedTokenException{
        Token t1 = getToken();
		Node n = new Node(node);
        if(t1.getCl() == TokenClass.comma){
            Token t2 = getToken();
            if(t2.getCl() == TokenClass.intVal){
				n.append(new Node(NodeClass.intVal, t2.getValue()));
                Token t3 = getToken();
                if(t3.getCl() == TokenClass.semicolon){
					i.append(n);
                    TokenClass c = getTokenClass();
                    if(c == TokenClass.dDevant ||
                            c == TokenClass.dGauche ||
                            c == TokenClass.dDroite ||
							c == TokenClass.dArriere ||
                            c == TokenClass.dZero){
                        getToken();
                        return Mouvement(i);
                    }
                    if (c == TokenClass.rightPar) {
                        return i;
                    }
                }
            }
        }
        return null;
    }
    private Node Mouvement(Node i) throws UnexpectedTokenException {

        getBackToken();
        Token t = getToken();
        if(t.getCl() == TokenClass.dDevant) {
            MouvementF(i, NodeClass.dDevant);
        }
        if(t.getCl() == TokenClass.dDroite) {
            MouvementF(i, NodeClass.dDroite);
        }
        if(t.getCl() == TokenClass.dGauche){
            MouvementF(i, NodeClass.dGauche);
        }
        if(t.getCl() == TokenClass.dZero){
            MouvementF(i, NodeClass.dZero);
        }
		if(t.getCl() == TokenClass.dArriere){
            MouvementF(i, NodeClass.dArriere);
        }
        if (getTokenClass() == TokenClass.rightPar){
			return i;
        }
        throw new UnexpectedTokenException("Mouvement");
    }

}
