package game;

import java.util.ArrayList;
import java.util.List;

public class AnalyseLexicale {

	/*
	Table de transition de l'analyse lexicale
	 */
	private static Integer TRANSITIONS[][] = {
		 //             espace    lettre      chiffre    =          >         ,       ;       (        )
		 /*  0 */    {      0,       1,        2,        3,        404,      101,    102,    103,     104   },
		 /*  1 */    {    201,       1,        1,       201,       201,      201,    201,    201,     201   },
		 /*  2 */    {    202,      202,       2,       202,       202,      202,    202,    202,     202   },
		 /*  3 */    {    404,      404,      404,      404,       203,      404,    404,    404,     404   },

		 // 201 accepte identifiant ou mot clé   (goBack : oui)
		 // 202 accepte entier                   (goBack : oui)
		 // 203 accepte =>                       (goBack : non)
		 // 404 rejete
	};


	private static final int ETAT_INITIAL = 0;

	/*
	Pour chaque symbole terminal acceptable en entrée de l'analyse syntaxique
	retourne un indice identifiant soit un symbole, soit une classe de symbole :
	  - 0 : espace ou assimilé
	  - 1 : lettre
	  - 2 : chiffre
	 */
	private static int indiceSymbole(Character c) throws IllegalCharacterException {
		if (c == null) return 0;
		if (Character.isWhitespace(c)) return 0;
		if (Character.isLetter(c)) return 1;
		if (Character.isDigit(c)) return 2;
		if (c == '=') return 3;
		if (c == '>') return 4;
		if (c == ',') return 5;
		if (c == ';') return 6;
		if (c == '(') return 7;
		if (c == ')') return 8;
		throw new IllegalCharacterException("Le symbole "+c.toString()+" est inconnue du langage");
	}

	/*
	effectue l'analyse lexicale et retourne une liste de Token
	 */
	public static List<Token> analyse(String entree) throws Exception {
		List<Token> tokens = new ArrayList<>();

		/* copie des symboles en entrée
		- permet de distinguer les mots-clés des identifiants
		- permet de conserver une copie des valeurs particulières des tokens de type ident et intval
		 */
		String buf = "";

		Integer etat = ETAT_INITIAL;
		SourceReader sr = new SourceReader(entree);

		Character c;
		do {
			c = sr.lectureSymbole();
			Integer e = TRANSITIONS[etat][indiceSymbole(c)];
			if (e == null) {
				System.out.println("pas de transition depuis état " + etat + " avec symbole " + c);
				throw new LexicalErrorException("pas de transition depuis état " + etat + " avec symbole " + c);
			}
			// cas particulier lorsqu'un état d'acceptation à état atteint
			if (e >= 100) {
				if (e == 101) {
					tokens.add(new Token(TokenClass.comma));
				} else if (e == 102) {
					tokens.add(new Token(TokenClass.semicolon));
				} else if (e == 103) {
					tokens.add(new Token(TokenClass.leftPar));
				} else if (e == 104) {
					tokens.add(new Token(TokenClass.rightPar));
				} else if (e == 201) {
					if (buf.equals("tuile")) {
						tokens.add(new Token(TokenClass.kTuile));
					} else if (buf.equals("fantome")) {
						tokens.add(new Token(TokenClass.kFantome));
					} else if (buf.equals("visite")) {
						tokens.add(new Token(TokenClass.kVisite));
					} else if (buf.equals("devant")) {
						tokens.add(new Token(TokenClass.dDevant));
					} else if (buf.equals("arriere")) {
						tokens.add(new Token(TokenClass.dArriere));
					} else if (buf.equals("gauche")) {
						tokens.add(new Token(TokenClass.dGauche));
					} else if (buf.equals("droite")) {
						tokens.add(new Token(TokenClass.dDroite));
					} else if (buf.equals("zero")) {
						tokens.add(new Token(TokenClass.dZero));
					} else if (buf.equals("vide")) {
						tokens.add(new Token(TokenClass.tVide));
					} else if (buf.equals("mur")) {
						tokens.add(new Token(TokenClass.tMur));
					}
					sr.goBack();
				} else if (e == 202) {
					tokens.add(new Token(TokenClass.intVal, buf));
					sr.goBack();
				}
				 else if (e == 203) {
					tokens.add(new Token(TokenClass.to));
				}
				 else if (e == 404) {
					 System.out.println("pas de transition depuis état " + etat + " avec symbole " + c);
					 throw new LexicalErrorException("pas de transition depuis état " + etat + " avec symbole " + c);
				}
				// un état d'acceptation ayant été atteint, retourne à l'état 0
				etat = 0;
				// reinitialise buf
				buf = "";
			} else {
				// enregistre le nouvel état
				etat = e;
				// ajoute le symbole qui vient d'être examiné à buf
				// sauf s'il s'agit un espace ou assimilé
				if (etat>0) buf = buf + c;
			}

		} while (c != null);

		return tokens;
	}

}
