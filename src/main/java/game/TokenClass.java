package game;

public enum TokenClass {
	intVal,
	to, semicolon,
	leftPar, rightPar,
	comma,
	dDevant, dDroite, dGauche, dZero, dArriere, //Directions
	kTuile, kFantome, kVisite, // Fonctions
	tMur, tVide // Tuile
}
