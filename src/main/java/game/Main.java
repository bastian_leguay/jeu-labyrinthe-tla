package game;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Stage secondaryStage = new Stage();
        Stage errorStage = new Stage();
        // fenêtre principale et panneau de menu

        GridPane menuPane = new GridPane();
        Button btnLevel1 = new Button("Niveau 1 : Visite d'une tuile");
        menuPane.add(btnLevel1, 0, 1);
        Button btnLevel2 = new Button("Niveau 2 : Fantome mouvement aléatoire");
        menuPane.add(btnLevel2, 0, 2);



        TextArea textfield = new TextArea("tuile(14,20;;1=>19;;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;)\nvisite(1,0) => 2,0 mur;visite(1,0) => 0,1 vide;visite(1,0) => 1,1 vide;\nfantome(6,4,1,(devant,4;droite,1;devant,1;zero,1;))");
        textfield.setPrefWidth(500);
        menuPane.add(textfield, 0, 4);

        Button btnTest = new Button("Niveau test : Création du niveau");
        menuPane.add(btnTest, 0, 5);


        ImageView imageView = new ImageView(SpritesLibrary.imgPlayerLarge);
        menuPane.add(imageView, 1, 0, 1, 6);


//        Panel pour l'explication des régles de création d'un niveau

        GridPane menuElement = new GridPane();
        menuElement.autosize();
        Scene sceneElement = new Scene(menuElement);
        Label l1 = new Label("Voici le language du jeu pour la creation de niveau.\n");
        menuElement.add(l1,1,1);
        Label l2 = new Label("Cette creation se fait en 3 étapes :");
        menuElement.add(l2,1,2);

//      Suite de Label permettant l'affichage des informations

        Label l5 = new Label("Se jeu est un plateau de 14*20  (ligne*colonne).\n" +
                "Le terrain se créé avec le terme: \"tuile()\".\n" +
                "Ca prend forme de la manière suivante tuile(Sortie ; Bloc).\n" +
                "La tuile de sortie se fait en premier lieu en marquant une coordonnée de la forme suivante : \"ligne , colonne ;\"\n" +
                "Pour chaque ligne de code, les tuiles \"blocs\" se font de la façon suivante:\n" +
                "-\"numéro de colonne , numéro de colonne , etc ,\" ce qui aura pour conséquence de mettre des blocs un par un.\n" +
                "-\"numéro de colonne => numéro de colonne\"  ce qui permet la création d'une suite de blocs depuis la première colonne indiquée jusqu'à   \n la dernière.\n" +
                "Il faut savoir que les deux méthodes de création de bloc sont compatibles et peuvent être utilisées dans la même ligne.\n\n" +
                "Pour changer de ligne vous devez mettre: \";\".\n" +
                "Si vous oubliez de mettre des éléments dans une ligne ou que vous faites \";;\"\n elle sera considérée comme vide de tous blocs.\n" +
                "Si vous ajoutez plus de lignes et colonnes que ce que le plateau peut contenir, \nsoit 14 lignes sur 20 colonnes, alors cela ne sera pas pris en compte dans le jeu.\n" +
                "Exemple: tuile(14,20;4,8,9;1=>19;;2=>20;;1=>19;5,6;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;);\n\n");
        menuElement.add(l5,1,3);

        Label l6 = new Label("Maintenant nous allons décrire la création de fantôme.\n" +
                "Il y a deux types de fantôme: \n" +
                "Le fantôme à déplacement libre et celui à déplacement aléatoire.\n" +
                "Pour ajouter un fantôme, vous devez écrire \"fantome()\".\n" +
                "Cela prend forme de cette manière : \"fantome(ligne , colonne , orientation , (déplacement))\".\n" +
                "Les deux premières valeurs sont donc des coordonnées à écrire.\n" +
                "La troisième valeur est la direction que prendra le fantôme: haut = 0, droite = 1, bas = 2, gauche = 3.\n" +
                "Ensuite les parenthèses sont les déplacements du fantôme. Il y en a 5: devant, droite, gauche, arriere, zero.\n" +
                "Pour mettre en place le déplacement, il faut faire: \"déplacement , nombre de case ; déplacement , nombre de case ; etc ;\"\n" +
                "Si vous voullez renvoyer un fantôme à sa première position, il faut utiliser le terme: \"zero , nombre\".\n" +
                "Le nombre après zero est le temps qu'il restera sur la première case avant de se déplacer de nouveau.\n" +
                "Exemple de déplacement : (devant,4;droite,1;devant,1;zero,1;)\n\n" +
                "Pour créer un fantôme au déplacement aléatoire, vous devez simplement choisir la position de départ et la direction.\n" +
                "Soit \"fantôme(ligne , colonne , orientation)\"\n" +
                "Pour les deux fantômes, leurs déplacments sont des boucles qu'ils executeront à l'infini.\n" +
                "Exemple: fantome(6,4,1,(devant,4;droite,1;devant,1;zero,1;))fantome(2,2,0)\n\n");
         menuElement.add(l6,1,4);

        Label l7 = new Label("Enfin, la dernière chose paramètrable est le fait de faire apparaître ou disparaître un bloc en passant sur une case.\n" +
                "Pour se faire il faut écrire: \"visite()\"\n" +
                "Vous devez écrire de la manière suivante: \"visite(ligne , colonne) => ligne , colone  Etat\".\n" +
                "La première écriture de \"ligne , colonne\" est la tuile sur laquelle il faut être pour que cela s'active.\n" +
                "La seconde écriture de \"ligne , colonne\" est la tuile qui sera changée.\n" +
                "Etat est la tuile qui sera changée:\n" +
                "-si un bloc doit être transformé en tuile vide: \"vide\"\n" +
                "-si une tuile vide doit être transformée en bloc: \"mur\"\n" +
                "Exemple: visite(1,0) => 2,0 mur;visite(1,0) => 0,1 vide;visite(1,0) => 1,1 vide;");
        menuElement.add(l7,2,3);


        Label l8 = new Label("Enfin pour créer un niveau complet vous pouvez utilisé les éléments que vous souhaitez\n" +
                "Si vous ecrivez tuile(quelque chose)tuile(quelque chose)\n" +
                "Seul la dernière tuile() ajoutée sera utilisée.\n" +
                "Pour les autres éléments vous pouvez en mettre autant que vous en voulez !\n\n" +
                "Exemple global: tuile(14,20;4,8,9;1=>19;;2=>20;;1=>19;5,6;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;)\n" +
                "fantome(6,4,1,(devant,4;droite,1;devant,1;zero,1;))fantome(2,2,0)\n" +
                "visite(1,0) => 2,0 mur;visite(1,0) => 0,1 vide;visite(1,0) => 1,1 vide;");

        menuElement.add(l8,2,4);

//      Mise d'un format pour les label, pour une meilleur lecture
        l1.setFont(new Font("Arial", 16));
        l2.setFont(new Font("Arial", 13));
        l5.setFont(new Font("Arial", 13));
        l6.setFont(new Font("Arial", 13));
        l7.setFont(new Font("Arial", 13));
        l8.setFont(new Font("Arial", 13));

        secondaryStage.setScene(sceneElement);
        secondaryStage.show();

        Scene scene = new Scene(menuPane);
        primaryStage.setScene(scene);
        primaryStage.show();


        GridPane errorPanel = new GridPane();
        menuElement.autosize();
        Scene errorScene = new Scene(errorPanel);
        errorStage.setScene(errorScene);

        // panneau racine du jeu

        BorderPane gamePane = new BorderPane();

        Game game = new Game(gamePane);

        btnLevel1.setOnAction(event -> {
            // affiche le panneau racine du jeu (à la place du panneau de menu)
            scene.setRoot(gamePane);

            // Utilise l'interpreteur pour la creation du niveau
            try {
                game.setLevel(new Interpreteur("tuile(7,10;4,9,11,13,14,16,18;2,4,9,14,16=>19;4=>7,9,10,12,18;2,7,9,10,12h=>15;2,4,7,15=>19;1,2,4,5,9,10,11,14,15,19;1,5,7,8,9,11,15,16,17,19;1,3,7,9,10,11,13,14,15,17;5,6,7,10,17,18,19;2=>5,12=>17,19;2,9=>12;2=>9,14,18,19;2,3,8,11,13,14,16,18;5,6,10,11,16,18;;)"+
                "visite(11,1)=>8,5 vide;visite(11,1)=>9,5 vide;visite(11,1)=>10,5 vide;visite(11,1)=>8,6 vide;visite(11,1)=>10,6 vide;visite(11,1)=>8,7 vide;visite(11,1)=>9,7 vide;visite(11,1)=>10,7 vide;"+
                "fantome(8,5,1,(devant,2;droite,1;))fantome(10,5,2,(devant,2;droite,1;))fantome(8,7,0,(devant,2;droite,1;))fantome(10,7,3,(devant,2;droite,1;))"));
            }
            //Si il y a un caratère qui n'est pas prit en compte par le langage
            catch (Exception e) {
                e.printStackTrace();
            }
            // démarre le jeu
            game.start();

            // ajuste la taille de la fenêtre
            primaryStage.sizeToScene();

        });

        btnLevel2.setOnAction(event -> {
            scene.setRoot(gamePane);

            try {
                game.setLevel(new Interpreteur("tuile(14,20;;1=>19;;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;;1=>19;;2=>20;)visite(1,0) => 2,0 mur;visite(1,0) => 0,1 vide;visite(1,0) => 1,1 vide;fantome(6,4,1)fantome(13,8,3)fantome(7,11,0)"));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            game.start();
            primaryStage.sizeToScene();
        });



        btnTest.setOnAction(event -> {
            String textCreationNiveau = textfield.getText();
            scene.setRoot(gamePane);
            try {
                game.setLevel(new Interpreteur(textCreationNiveau));
              }
            catch (Exception e) {

                Label error = new Label(e.getMessage());
                errorPanel.add(error,1,1);
                errorStage.show();
            }
            game.start();
            primaryStage.sizeToScene();
        });

        // gestion du clavier

        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.Q) {
                // touche q : quitte le jeu et affiche le menu principal
                game.stop();
                scene.setRoot(menuPane);
                primaryStage.sizeToScene();
            }
            if (event.getCode() == KeyCode.R) {
                // touche r : redémarre le niveau en cours
                game.start();
            }
            if (event.getCode() == KeyCode.LEFT) {
                game.left();
            }
            if (event.getCode() == KeyCode.RIGHT) {
                game.right();
            }
            if (event.getCode() == KeyCode.UP) {
                game.up();
            }
            if (event.getCode() == KeyCode.DOWN) {
                game.down();
            }
        });
    }
}
