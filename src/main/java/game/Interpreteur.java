package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;

public class Interpreteur implements Level {

	// permet la lecture de chaîne au clavier
	static BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));

	// stocke les variables lues au clavier durant l'interprétation
	private HashMap<String, Integer> variables;
    private Node tuile, fantome, visite;

	public Interpreteur(String entree) throws Exception {
        this.variables = new HashMap<>();
        List<Token> tokens = AnalyseLexicale.analyse(entree);
        Node root = new AnalyseSyntaxique().analyse(tokens);
        Node tuile = null, fantome = new Node(NodeClass.A), visite = new Node(NodeClass.A);
        for (int i=0; i<root.getChildrenCount(); i++) {
            switch (root.getChild(i).getChild(0).getCl()) {
                case kTuile: {
                    tuile = root.getChild(i).getChild(0);
                    break;
                }
                case kVisite: {
                    visite.append(root.getChild(i).getChild(0));
                    break;
                }
                case kFantome: {
                    fantome.append(root.getChild(i).getChild(0));
                    break;
                }
            }
        }
        this.tuile = tuile;
        this.fantome = fantome;
        this.visite = visite;

    }

    public static List<?> convertObjectToList(Object obj) {
    List<?> list = new ArrayList<>();
    if (obj.getClass().isArray()) {
        list = Arrays.asList((Object[])obj);
    } else if (obj instanceof Collection) {
        list = new ArrayList<>((Collection<?>)obj);
    }
    return list;
}

	/*
	interprete le noeud n
	et appel récursif sur les noeuds enfants de n

	retourne
	  null si le noeud est une instruction (kPrint ou kInput)
	  la valeur de l'expression si le noeud est une expression

	 */
	Object interpreter(Node n) {

		switch(n.getCl()) {
			case statement:
				interpreter(n.getChild(0));
				if (n.getChildrenCount()>1) {
					interpreter(n.getChild(1));
				}
				return null;
			case A:{
                ArrayList<List<Integer> > res = new ArrayList<>();
				for(int i = 0; i< n.getChildrenCount(); i++){
                    res.add((List<Integer>) convertObjectToList(interpreter(n.getChild(i))));
                }
                return res;
			}
			case kTuile:{
				StringBuilder space = new StringBuilder(new String(new char[280]).replace('\0', ' '));
                // Ici je génère la liste mur
                ArrayList<List<Integer> > mur = new ArrayList<>(15);
                List<Integer> sortie = (List<Integer>) convertObjectToList(interpreter(n.getChild(0)));
                List<List<Integer>> lignes = (List<List<Integer>>) convertObjectToList(interpreter(n.getChild(1)));

                mur.add(sortie);
                mur.addAll(lignes);

                int ligne=1;
                for(int l = 0; l< space.length(); l+=20){ // Chaque ligne donc 13 iterations
                    for(int i = 0; i<mur.get(ligne).size(); i++){ // i = chaque mur
                        space.setCharAt(l+mur.get(ligne).get(i)-1, '#');
                    }
                    ligne ++;
                }
                space.setCharAt((mur.get(0).get(0)-1)*20+(mur.get(0).get(1)-1), '*');
                return space.toString();

			}
            case Position:{
                return Arrays.asList(interpreter(n.getChild(0)),interpreter(n.getChild(1)));
            }
			case kFantome:{
                ArrayList<Integer> fantome = new ArrayList<>();
                fantome.add((Integer) interpreter(n.getChild(0)));
                fantome.add((Integer) interpreter(n.getChild(1)));
                fantome.add((Integer) interpreter(n.getChild(2)));
                if(n.getChildrenCount() <= 3){ // Si il n'y a pas de mouvement on ajoute des mouvements aléatoires
                    fantome.addAll(mouvementAleatoire());
                }
                else fantome.addAll((List<Integer>) interpreter(n.getChild(3)));
                return fantome;
			}
			case kVisite:{

                ArrayList<Integer> visite=new ArrayList<>();
                visite.addAll((List<Integer>) interpreter(n.getChild(0)));

                visite.addAll((List<Integer>) interpreter(n.getChild(1)));

                if(n.getChild(2).getCl() == NodeClass.tMur) {
                    visite.add(0);
                }
                if(n.getChild(2).getCl() == NodeClass.tVide) {
                    visite.add(1);
                }

                return visite;
            }
			case Mur:{
                ArrayList<List<Integer> > res = new ArrayList<>();
                List<Integer> l1 = new ArrayList<>();
                List<Integer> l2 = new ArrayList<>();
                List<Integer> l3 = new ArrayList<>();
                List<Integer> l4 = new ArrayList<>();
                List<Integer> l5 = new ArrayList<>();
                List<Integer> l6 = new ArrayList<>();
                List<Integer> l7 = new ArrayList<>();
                List<Integer> l8 = new ArrayList<>();
                List<Integer> l9 = new ArrayList<>();
                List<Integer> l10 = new ArrayList<>();
                List<Integer> l11 = new ArrayList<>();
                List<Integer> l12 = new ArrayList<>();
                List<Integer> l13 = new ArrayList<>();
                List<Integer> l14 = new ArrayList<>();
                res.add(l1);
                res.add(l2);
                res.add(l3);
                res.add(l4);
                res.add(l5);
                res.add(l6);
                res.add(l7);
                res.add(l8);
                res.add(l9);
                res.add(l10);
                res.add(l11);
                res.add(l12);
                res.add(l13);
                res.add(l14);
                int l = 0;
                int i =0;
                while(i<n.getChildrenCount()-1 && l<14){ // -1 car les enfants de mur finisent toujours par un semicolon, l<14 permet si il y a plus de 15 ligne dans le language d'arreter à la 14eme ligne
                    //Ajoute la valeur dans <bloc> à la liste
                    res.get(l).addAll((List<Integer>) convertObjectToList(interpreter(n.getChild(i))));
                    if(n.getChild(i).getChild(n.getChild(i).getChildrenCount()-1).getCl() == NodeClass.semicolon){
                        //Si c'est la fin d'une ligne on ajoute la liste à res et on remet à zero la liste
                        l++;
                    }
                    i++;
                }
                return res;
			}
			case bloc:{
                List<Integer> res = new ArrayList<>();
                if(n.getChild(0).getCl() == NodeClass.intVal){
                    if(n.getChildrenCount() >= 2 && n.getChild(1).getCl() == NodeClass.intVal){
                        for (int i = (Integer) interpreter(n.getChild(0)); i<(Integer) interpreter(n.getChild(1))+1; i++){
                            res.add(i);
                        }
                    }
                    res.add((Integer)interpreter(n.getChild(0)));
                }
                return res;
			}
			case Mouvement:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<n.getChildrenCount(); i++){
                    res.addAll((List<Integer>) interpreter(n.getChild(i)));
                }
                return res;
			}
			case dDevant:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<(Integer)interpreter(n.getChild(0));i++){
                    res.add(1);
                }
                return res;
			}
			case dDroite:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<(Integer)interpreter(n.getChild(0));i++){
                    res.add(2);
                }
                return res;
			}
			case dGauche:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<(Integer)interpreter(n.getChild(0));i++){
                    res.add(3);
                }
                return res;
			}
			case dZero:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<(Integer)interpreter(n.getChild(0));i++){
                    res.add(0);
                }
                return res;
			}
            case dArriere:{
                List<Integer> res = new ArrayList<>();
                for(int i =0; i<(Integer)interpreter(n.getChild(0));i++){
                    res.add(4);
                }
                return res;
			}

            case intVal:{
                /* retourne la valeur d'entier litéral */
				return Integer.valueOf(n.getValue());
            }
		}
		return null;
	}

    private Collection<Integer> mouvementAleatoire() {
        List<Integer> res = new ArrayList<>();
        int max = 4;
        int min = 1;
        int range = max - min + 1;

        int nbMouvement = 20;

        int prec = 0;
        for (int i = 0; i < nbMouvement; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand == prec && (prec==2 || prec ==3)){
                // Si le mouvement est le même que le dernier ET est un turn on retire un mouvement
                i--;
            }
            else {
                res.add(rand);
                prec = rand;
            }
        }
        res.add(0);

        return res;
    }

    @Override
    public char[] getWalls() {
        if (this.tuile != null) return interpreter(this.tuile).toString().toCharArray();
        return new char[0];
    }

    @Override
    public ArrayList<Ghost> getGhosts() {
        if (true) {
        List<List<Integer>> fantomes = (List<List<Integer>>) convertObjectToList(interpreter(this.fantome));

            ArrayList<Ghost> ghosts = new ArrayList<>();
            for (int i = 0; i < fantomes.size(); i++) {
                GhostAction[] bar = new GhostAction[fantomes.get(i).size()-3];
                for(int j = 3; j<fantomes.get(i).size(); j++){
                    switch (fantomes.get(i).get(j)){
                        case 0:
                            bar[j-3] = GhostAction.REINIT;
                            break;
                        case 1:
                            bar[j-3] = GhostAction.FORWARD;;
                            break;
                        case 2:
                            bar[j-3] = GhostAction.TURN_RIGHT;;
                            break;
                        case 3:
                            bar[j-3] = GhostAction.TURN_LEFT;;
                            break;
                        case 4:
                            bar[j-3] = GhostAction.BACK;;
                            break;
                    }
                }
                ghosts.add(
                        new Ghost(
                                fantomes.get(i).get(0),
                                fantomes.get(i).get(1),
                                fantomes.get(i).get(2),
                                bar
                        )
                );
            }
            return ghosts;
        }
        return Level.super.getGhosts();
    }

    @Override
    public void adjustWalls(Game game) {
        List<List<Integer>> visites = (List<List<Integer>>) convertObjectToList(interpreter(this.visite));
        for (int i = 0; i < visites.size(); i++) {

            Integer x = visites.get(i).get(0);
            Integer y = visites.get(i).get(1);

            Integer x1 = visites.get(i).get(2);
            Integer y1 = visites.get(i).get(3);

            Integer tile = visites.get(i).get(4);
            if (tile == 0) {
                game.getTile(x1, y1).setState(
                        game.isVisited(x, y) > 0 ?
                                TileState.WALL :
                                TileState.EMPTY
                );
            }
            if (tile == 1) {

                game.getTile(x1, y1).setState(
                        game.isVisited(x, y) > 0 ?
                                TileState.EMPTY :
                                TileState.WALL
                );
            }

        }
    }
}
