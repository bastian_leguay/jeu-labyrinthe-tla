package game;

/*
Enumération des actions possibles d'un fantôme
*/
public enum GhostAction {
    REINIT,      // replace le fantôme à sa position initiale,
    FORWARD,     // avance d'un carreau
    TURN_RIGHT,  // tourne à droite
    TURN_LEFT,   // tourne à gauche
    BACK,         // recule d'un carreau
};
